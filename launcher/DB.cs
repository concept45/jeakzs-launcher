﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Net.Sockets;
using System.Windows.Forms;
namespace launcher
{
    class DB
    {
        public static System.Drawing.Color bColor= System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
        
        public static string CONN_STRING_AUTH = "SERVER=" + SIMPLE_CONFIG.IP + ";" +
                        "DATABASE=" + SIMPLE_CONFIG.AUTH_DB + ";" +
                        "UID=" + SIMPLE_CONFIG.USER_DB + ";" +
                        "PASSWORD=" + SIMPLE_CONFIG.PASSWORD_DB + ";";
        public static string CONN_STRING_CHAR = "SERVER=" + SIMPLE_CONFIG.IP + ";" +
                        "DATABASE=" + SIMPLE_CONFIG.CHARACTERS_DB + ";" +
                        "UID=" + SIMPLE_CONFIG.USER_DB + ";" +
                        "PASSWORD=" + SIMPLE_CONFIG.PASSWORD_DB + ";";
        public static string CONN_STRING_LAUNCHER = "SERVER=" + SIMPLE_CONFIG.IP + ";" +
                        "DATABASE=" + SIMPLE_CONFIG.LAUNCHER_DB + ";" +
                        "UID=" + SIMPLE_CONFIG.USER_DB + ";" +
                        "PASSWORD=" + SIMPLE_CONFIG.PASSWORD_DB + ";";
        public static string GetRealmlist()
        {
            string realm = Properties.Settings.Default.server;
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_AUTH);
                MySqlCommand cmd = new MySqlCommand("select address from realmlist", conn);
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                realm = reader["address"].ToString();
                conn.Close();
            }
            catch { }
            Properties.Settings.Default.server = realm;
            Properties.Settings.Default.Save();
            return realm;
        }
        public static string GetRealmName()
        {
            string realm = Properties.Settings.Default.server;
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_AUTH);
                MySqlCommand cmd = new MySqlCommand("select name from realmlist", conn);
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                realm = reader["name"].ToString();
                conn.Close();
            }
            catch { }
            Properties.Settings.Default.server = realm;
            Properties.Settings.Default.Save();
            return realm;
        }
        public static int GetOnlinePlayers()
        {
            int online = 0;
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_CHAR);
                MySqlCommand cmd = new MySqlCommand("select guid from characters where online=1", conn);
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    online++;
                conn.Close();
            }
            catch { }
            return online;
        }
        public static bool ServerOnline(string ip, int port)
        {
            if (ip == null)
                throw new ArgumentNullException("host");

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                socket.Connect(ip, port);
                return socket.Connected;
            }
            catch
            {
                return false;
            }
        }
        public static string GetNews()
        {
            string news = Properties.Settings.Default.news;
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_LAUNCHER);
                MySqlCommand cmd = new MySqlCommand("select news from data", conn);
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                news = reader["news"].ToString();
                conn.Close();
            }
            catch { }
            Properties.Settings.Default.news = news;
            Properties.Settings.Default.Save();
            return news;
        }
        public static string GetChangelog()
        {
            string changelog = Properties.Settings.Default.changelog;
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_LAUNCHER);
                MySqlCommand cmd = new MySqlCommand("select changelog from data", conn);
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                changelog = reader["changelog"].ToString();
                conn.Close();
            }
            catch { }
            Properties.Settings.Default.changelog = changelog;
            Properties.Settings.Default.Save();
            return changelog;
        }
        public static void GetLinks(out string _register, out string _forum, out string _account, out string _changelog, out string _news)
        {
            _register = Properties.Settings.Default.register;
            _forum = Properties.Settings.Default.forum;
            _account = Properties.Settings.Default.account;
            _changelog = Properties.Settings.Default.changelog_link;
            _news = Properties.Settings.Default.news_link;
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_LAUNCHER);
                MySqlCommand cmd = new MySqlCommand("select * from settings", conn);
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _register = reader["register"].ToString();
                    _forum = reader["forum"].ToString();
                    _account = reader["account"].ToString();
                }
                conn.Close();
                cmd = new MySqlCommand("select * from data", conn);
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _changelog = reader["changelog_link"].ToString();
                    _news = reader["news_link"].ToString();
                }
                conn.Close();
            }
            catch { }
            Properties.Settings.Default.register = _register;
            Properties.Settings.Default.forum = _forum;
            Properties.Settings.Default.account = _account;
            Properties.Settings.Default.changelog_link = _changelog;
            Properties.Settings.Default.news_link = _news;
            Properties.Settings.Default.Save();
            
        }
        public static void GetPatch(out int _patch_version, out string _patch_name,out string _patch_link)
        {
            _patch_version = Properties.Settings.Default.patch_version;
            _patch_name = Properties.Settings.Default.patch_name;
            _patch_link = "";
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_LAUNCHER);
                MySqlCommand cmd = new MySqlCommand("select * from settings", conn);
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _patch_version = Convert.ToInt32(reader["patch_version"]);
                    _patch_name = reader["patch_name"].ToString();
                    _patch_link = reader["patch_link"].ToString();
                }
                conn.Close();
            }
            catch { }
        }
        public static bool PlayerExists(string name)
        {
            int exists = 0;
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_CHAR);
                MySqlCommand cmd = new MySqlCommand("select guid from characters where name=@name", conn);
                cmd.Parameters.AddWithValue("@name", char.ToUpper(name[0]) + name.Substring(1));
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    exists++;
                conn.Close();
            }
            catch { }
            return (exists != 0);
        }
        public static Character AddCharacterData(string _NAME)
        {
        int _LEVEL=0, _RACE=0, _GENDER=0, _CLASS=0, _KILLS=0, _ONLINE=0;
            try
            {
                MySqlConnection conn = new MySqlConnection(CONN_STRING_CHAR);
                MySqlCommand cmd = new MySqlCommand("select level,race,gender,class,totalkills,online from characters where name=@name", conn);
                cmd.Parameters.AddWithValue("@name", char.ToUpper(_NAME[0]) + _NAME.Substring(1));
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _LEVEL = Convert.ToInt32(reader["level"]);
                    _RACE = Convert.ToInt32(reader["race"]);
                    _GENDER = Convert.ToInt32(reader["gender"]);
                    _CLASS = Convert.ToInt32(reader["class"]);
                    _KILLS = Convert.ToInt32(reader["totalkills"]);
                    _ONLINE = Convert.ToInt32(reader["online"]);
                }
                conn.Close();
            }
            catch { }
            return new Character(_NAME, _LEVEL, _RACE, _GENDER, _CLASS, _KILLS, _ONLINE);
        }
    }
    class Character // WORK IN PROGRESS. NOT YET IMPLEMENTED
    {
        string _NAME;
        int _LEVEL, _RACE, _GENDER, _CLASS, _KILLS, _ONLINE;
        public Character(string _name, int _level, int _race, int _gender, int _class, int _kills,int _online)
        {
            _NAME = char.ToUpper(_name[0]) + _name.Substring(1);
            _LEVEL = _level;
            _GENDER = _gender;
            _RACE = _race;
            _CLASS = _class;
            _KILLS = _kills;
            _ONLINE = _online;
        }
        public void Show(Label lblName, PictureBox picStatus )
        {
            lblName.Visible = true;
            picStatus.Visible = true;

            lblName.Text = _NAME;
            if (_ONLINE == 1)
                picStatus.Image = Properties.Resources.on;
            else picStatus.Image = Properties.Resources.off;
        }
        public void Reset(Label lblName, PictureBox picStatus)
        {
            lblName.Visible = false;
            picStatus.Visible = false;
        }
    }
}
