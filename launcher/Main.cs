﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace launcher
{
    public partial class Main : Form
    {

        string _REGISTER, _ACCOUNT, _FORUM, _NEWS, _CHANGELOG;
        System.Net.WebClient DOWNLOADER = new System.Net.WebClient();
        System.Threading.Thread Thread;
        bool SERVER_ONLINE, PLAYABLE = true, DOWNLOADING = false;
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }
            base.WndProc(ref m);
        }
        public Main()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.Opaque, false);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
        }
        private void Main_Load(object sender, EventArgs e)
        {
            SERVER_ONLINE = DB.ServerOnline(SIMPLE_CONFIG.IP, SIMPLE_CONFIG.PORT);
            DB.GetLinks(out _REGISTER, out _FORUM, out _ACCOUNT, out _CHANGELOG, out _NEWS);
            if (_NEWS != "0")
                lblNewsReadMore.Visible = true;
            else lblNewsReadMore.Visible = false;
            if (_CHANGELOG != "0")
                lblChangelogReadMore.Visible = true;
            else lblChangelogReadMore.Visible = false;
            lblNewsData.Text = DB.GetNews();
            lblChangelogData.Text = DB.GetChangelog();
            lblRealmName.Text = DB.GetRealmName();
            if (SERVER_ONLINE)
            {
                picRealmStatus.Image = Properties.Resources.on;
                lblPlayersOnline.Text = DB.GetOnlinePlayers().ToString();
                DrawPlay(Properties.Resources.play_no_hover1);
            }
            else
            {
                lblPlayersOnline.Text = "0";
                picRealmStatus.Image = Properties.Resources.off;
                PLAYABLE = false;
                DrawPlay(Properties.Resources.play_no_active);
            }
        }
        private void lblExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(-1);
        }
        private void lblMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void lblMenu_MouseEnter(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            lbl.ForeColor = Color.LightGray;
        }
        private void lblMenu_MouseLeave(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            lbl.ForeColor = DB.bColor;
        }
        private void lblRegister_Click(object sender, EventArgs e)
        {
            if(_REGISTER!="0")
                System.Diagnostics.Process.Start(_REGISTER);
        }
        private void lblForum_Click(object sender, EventArgs e)
        {
            if (_FORUM != "0")
                System.Diagnostics.Process.Start(_FORUM);
        }
        private void lblAccount_Click(object sender, EventArgs e)
        {
            if (_ACCOUNT != "0")
                System.Diagnostics.Process.Start(_ACCOUNT);
        }
        private void picPlay_MouseEnter(object sender, EventArgs e)
        {
            if (SERVER_ONLINE && !DOWNLOADING)
                DrawPlay(Properties.Resources.play_hover);
            else DrawPlay(Properties.Resources.play_no_active);
        }
        private void picPlay_MouseDown(object sender, MouseEventArgs e)
        {
            if (SERVER_ONLINE && !DOWNLOADING)
                DrawPlay(Properties.Resources.play_take);
            else DrawPlay(Properties.Resources.play_no_active);
        }
        private void picPlay_MouseLeave(object sender, EventArgs e)
        {
            if (SERVER_ONLINE && !DOWNLOADING)
                DrawPlay(Properties.Resources.play_no_hover1);
            else DrawPlay(Properties.Resources.play_no_active);
        }
        private void picPlay_MouseUp(object sender, MouseEventArgs e)
        {
            if (SERVER_ONLINE && !DOWNLOADING)
                DrawPlay(Properties.Resources.play_hover);
            else DrawPlay(Properties.Resources.play_no_active);
        }
        private void picPlay_Click(object sender, EventArgs e)
        {
            if (PLAYABLE)
            {
                try
                {
                    System.Diagnostics.Process.Start("wow.exe");
                    Environment.Exit(-1);
                }
                catch {  }
            }
            else if (SERVER_ONLINE && !DOWNLOADING)
            {
                try
                {
                    DOWNLOADING = true;
                    int new_patch_version;
                    string cur_patch_name = Properties.Settings.Default.patch_name, new_patch_name, patch_link;
                    DB.GetPatch(out new_patch_version, out new_patch_name, out patch_link);
                    Thread = new System.Threading.Thread(() =>
                    {
                        DOWNLOADER.DownloadProgressChanged += new System.Net.DownloadProgressChangedEventHandler(DOWNLOADER_DownloadProgressChanged);
                        DOWNLOADER.DownloadFileCompleted += new AsyncCompletedEventHandler(DOWNLOADER_DownloadFileCompleted);
                        DOWNLOADER.DownloadFileAsync(new Uri(patch_link), "Data\\"+new_patch_name);
                    });
                    Thread.Start();
                }
                catch { }
            }
        }
        void DOWNLOADER_DownloadProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate {
                double bytesIn = double.Parse(e.BytesReceived.ToString());
                double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
                double percentage = bytesIn / totalBytes * 100;
                picTop2.BringToFront();
                picTop2.Size = new Size(Convert.ToInt32(6.48 * int.Parse(Math.Truncate(percentage).ToString())), 13) ;
            });
        }
        private void ConnectionCheck_Tick(object sender, EventArgs e)
        {
            //SERVER_ONLINE = DB.ServerOnline();
            //Main_Load(null, null);
        }
        private void lblSetRealmlist_Click(object sender, EventArgs e)
        {
            string[] locales = { "frFR", "deDE", "enGB", "enUS", "itIT", "koKR", "zhCN", "zhTW", "ruRU", "esES", "esMX", "ptBR" };
            foreach(string locale in locales)
            {
                if (System.IO.Directory.Exists("Data\\" + locale))
                {
                    System.IO.File.WriteAllText("Data\\" + locale + "\\realmlist.wtf", "set realmlist "+DB.GetRealmlist());
                }
            }
        }
        private void lblDeleteCache_Click(object sender, EventArgs e)
        {
            if (System.IO.Directory.Exists("Cache"))
            {
                System.IO.Directory.Delete("Cache",true);
            }
        }
        void DOWNLOADER_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate {
                picTop.BringToFront();
                DownloadComplete();
            });
        }
        private void picBNet_MouseEnter(object sender, EventArgs e)
        {
            picBNet.Image = Properties.Resources.img_bnetButton_hovered;
        }
        private void picBNet_MouseLeave(object sender, EventArgs e)
        {
            picBNet.Image = Properties.Resources.img_bnetButton_normal;
        }
        private void picPlayersOnline_MouseEnter(object sender, EventArgs e)
        {
            picPlayersOnline.Image = Properties.Resources.online_hover;
        }
        private void picPlayersOnline_MouseLeave(object sender, EventArgs e)
        {
            picPlayersOnline.Image = Properties.Resources.online;
        }
        private void picPlayersOnline_MouseDown(object sender, MouseEventArgs e)
        {
            picPlayersOnline.Image = Properties.Resources.online;
        }
        private void picPlayersOnline_MouseUp(object sender, MouseEventArgs e)
        {
            picPlayersOnline.Image = Properties.Resources.online_hover;
        }

        private void lblChangelogReadMore_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(_CHANGELOG);
        }

        private void lblNewsReadMore_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(_NEWS);
        }

        private void picPlayersOnline_Click(object sender, EventArgs e)
        {
            /*          WORKING ON IT
            if (SERVER_ONLINE)
            {
                Search src = new Search();
                src.ShowDialog();
            }
            */
        }
        void DrawPlay(Image IMG)
        {
            int cur_patch_version = Properties.Settings.Default.patch_version, new_patch_version, x = 0;
            string cur_patch_name = Properties.Settings.Default.patch_name, new_patch_name, BUTTON_TEXT, patch_link;
            DB.GetPatch(out new_patch_version, out new_patch_name, out patch_link);
            if (SERVER_ONLINE)
            {
                if (cur_patch_version != new_patch_version)
                {
                    x = 14;
                    BUTTON_TEXT = "UPDATE";
                    PLAYABLE = false;
                }
                else {
                    PLAYABLE = true;
                    BUTTON_TEXT = "PLAY";
                }
            }
            else BUTTON_TEXT = "PLAY";
            using (Font myFont = new Font("Century Gothic", 18))
            {
                Bitmap bitmap = new Bitmap(IMG, picPlay.Width, picPlay.Height);
                bitmap.MakeTransparent();
                Graphics graph = Graphics.FromImage(bitmap);

                graph.DrawString(BUTTON_TEXT, myFont, Brushes.White, new Point(70 - x, 5));
                picPlay.Image = bitmap;
                graph.Dispose();
            }
        }
        private void lblMenu_MouseDown(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            lbl.ForeColor = Color.DimGray;
        }
        private void lblMenu_MouseEnter(object sender, MouseEventArgs e)
        {
            Label lbl = (Label)sender;
            lbl.ForeColor = Color.LightGray;
        }
        private void lblMenu_MouseDown(object sender, MouseEventArgs e)
        {
            Label lbl = (Label)sender;
            lbl.ForeColor = Color.DimGray;
        } 
        void DownloadComplete()
        {
            DOWNLOADING = false;
            int new_patch_version;
            string new_patch_name,  patch_link;
            DB.GetPatch(out new_patch_version, out new_patch_name, out patch_link);
            Properties.Settings.Default.patch_version = new_patch_version;
            Properties.Settings.Default.patch_name = new_patch_name;
            Properties.Settings.Default.Save();
            DrawPlay(Properties.Resources.play_no_hover1);
        }
    }
}
