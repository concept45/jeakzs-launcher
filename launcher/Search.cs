﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace launcher
{
    public partial class Search : Form
    {
        Character CHARACTER;
        public Search()
        {
            InitializeComponent();
        }
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }
        private void Search_Load(object sender, EventArgs e)
        {
            DrawSearch(Properties.Resources.play_no_hover);
        }
        void DrawSearch(Image IMG)
        {
            using (Font myFont = new Font("Century Gothic", 12))
            {
                Bitmap bitmap = new Bitmap(IMG, picSearch.Width, picSearch.Height);
                bitmap.MakeTransparent();
                Graphics graph = Graphics.FromImage(bitmap);

                graph.DrawString("Search", myFont, Brushes.White, new Point(40, 3));
                picSearch.Image = bitmap;
                graph.Dispose();
            }
        }
        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void picSearch_Click(object sender, EventArgs e)
        {
            if (DB.PlayerExists(txtSearch.Text))
            {
                lblInfo.Text = "";
                CHARACTER = DB.AddCharacterData(txtSearch.Text);
                CHARACTER.Show(lblName, picPlayerStatus);
            }
            else lblInfo.Text = "Whoops! Character not found!";
        }
    }
}
